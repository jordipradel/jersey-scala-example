#Jersey Scala example

This project is a “hello world” implementation of a Scala REST API using Jersey JAX-RS and Jackson.

Read about it and my motivations on writing it at the companion [blog post](http://func.io/post/37179613925/scala-rest-api-with-jetty-jersey-jax-rs-jackson).