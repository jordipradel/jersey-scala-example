name := "Jersey Scala example"

version := "0.1"

scalaVersion := "2.9.2"

libraryDependencies ++= Seq(
    "org.apache.httpcomponents" % "httpclient" % "4.2.1",
	"org.eclipse.jetty" % "jetty-servlet" % "8.1.0.RC5",
	"com.sun.jersey" % "jersey-core" % "1.13",
	"com.sun.jersey" % "jersey-server" % "1.13",
	"com.sun.jersey" % "jersey-servlet" % "1.13",
	"com.sun.jersey" % "jersey-json" % "1.13",
	"javax.ws.rs" % "jsr311-api" % "1.1.1",
	"com.fasterxml" % "jackson-module-scala" % "1.9.3",
	"org.scalatest" %% "scalatest" % "1.8" % "test"
)