package server

import api.GreetingsResource
import com.agilogy.jersey.JacksonContextResolver
import org.codehaus.jackson.map.{SerializationConfig, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.codehaus.jackson.map.annotate.JsonSerialize
import util.Properties

object Context{

  private def intEnvOrElse(propertyName:String, defaultValue:Int) = Properties.envOrElse(propertyName, defaultValue.toString).toInt

  val webServerPort = intEnvOrElse("PORT",8080)

  val server = new Server(webServerPort)

  val mapper = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL)
    mapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS,false)
    //mapper.configure(SerializationConfig.Feature.INDENT_OUTPUT,true)
    mapper
  }

  val instances:Set[AnyRef] = Set[AnyRef](
    new GreetingsResource(),
    new JacksonContextResolver(mapper))

}