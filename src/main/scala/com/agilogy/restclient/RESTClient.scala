package com.agilogy.restclient

import org.apache.http.client.methods._
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.util.EntityUtils
import org.apache.http.client.utils.URIBuilder
import org.apache.http.entity.{ContentType, StringEntity}
import org.apache.http.{HttpResponse, HttpRequest, HttpEntityEnclosingRequest}

/** Minimal REST HTTP client (currently just used to run integration tests) */
class RESTClient(baseUri:String) {


  val httpClient = new DefaultHttpClient()
  val mimeType = "application/json"
  val encoding = "utf-8"

  private def getResponse(response: HttpResponse) = {
    response.getEntity match {
      case null => ""
      case responseBody => EntityUtils.toString(responseBody)
    }

  }

  private def uriRequest(verb:UriHTTPVerb, path:String, params:Map[String,String] = Map()) = {
    val uriBuilder = new URIBuilder(baseUri + path)
    params.foreach(param => uriBuilder.addParameter(param._1,param._2))
    val uri = uriBuilder.build()
    val request = verb match {
      case GET => new HttpGet(uri)
      case DELETE => new HttpDelete(uri)
    }
    val response = httpClient.execute(request)
    getResponse(response)
  }

  private def entityRequest(verb:EntityHTTPVerb, path:String, body: String=null) = {
    val uriBuilder = new URIBuilder(baseUri + path)
    val uri = uriBuilder.build()
    val request = verb match {
      case POST => new HttpPost(uri)
      case PUT => new HttpPut(uri)
    }
    if (body!=null){
      request.setEntity(new StringEntity(body,ContentType.create(mimeType,encoding)))
    }
    val response = httpClient.execute(request)
    getResponse(response)
  }

  def get(path:String, params:Map[String,String] = Map()) = uriRequest(GET, path, params)

  def delete(path:String, params:Map[String,String] = Map()) = uriRequest(DELETE, path, params)

  def post(path: String, body: String=null) = {
    entityRequest(POST,path, body)
  }

}

sealed trait HTTPVerb
sealed trait UriHTTPVerb extends HTTPVerb
sealed trait EntityHTTPVerb extends HTTPVerb
object GET extends UriHTTPVerb
object POST extends EntityHTTPVerb
object PUT extends EntityHTTPVerb
object DELETE extends UriHTTPVerb