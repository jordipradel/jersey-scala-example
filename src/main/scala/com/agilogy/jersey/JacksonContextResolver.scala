package com.agilogy.jersey

import javax.ws.rs.ext.{ContextResolver, Provider}
import javax.ws.rs.Produces
import scala.Array
import javax.ws.rs.core.MediaType
import org.codehaus.jackson.map.ObjectMapper

@Provider
@Produces(Array(MediaType.APPLICATION_JSON))
class JacksonContextResolver(mapper:ObjectMapper) extends ContextResolver[ObjectMapper]{

  def getContext(clazz: Class[_]) = mapper
}