package system

import com.agilogy.restclient.RESTClient
import org.scalatest.{BeforeAndAfterAll, FlatSpec}
import server.{Context, Server}

class JSONTest extends FlatSpec with BeforeAndAfterAll {

  val restClient = new RESTClient("http://localhost:8080")

  val JSON_EXAMPLE = """{"name":"Jordi","msg":"HELLO!!"}"""
  val JSON_EXAMPLE_WITH_COLLECTIONS = """{"names":["Jordi","John"],"msgs":["HELLO!!","HI!"]}"""
  val JSON_EXAMPLE_WITH_OPTIONAL_FIELDS =  """{"name":"Jordi","surname":null,"msg":"HELLO!!"}"""

	behavior of "JSON serialization"

  it should "serialize simple scala case classes into JSON body responses" in {
    val result = restClient.get("/greetings/hello")
    assert(result === JSON_EXAMPLE)
  }

  it should "deserialize JSON from request body into simple case class instances" in {
    val result = restClient.post("/greetings/", body=JSON_EXAMPLE)
    assert(result === JSON_EXAMPLE)
  }

  it should "serialize scala case classes with collections" in {
    val result = restClient.get("/greetings/withCollections")
    assert(result === JSON_EXAMPLE_WITH_COLLECTIONS)
  }

  it should "serialize scala case classes with Option" in {
    val result = restClient.get("/greetings/withOption")
    assert(result === JSON_EXAMPLE_WITH_OPTIONAL_FIELDS)
  }

  it should "deserialize JSON with null into scala case classes with Option as None" in {
    val result = restClient.post("/greetings/withOption", body=JSON_EXAMPLE_WITH_OPTIONAL_FIELDS)
    assert(result === "Got None")
  }

  it should "deserialize JSON without property into scala case classes with Option as None" in {
    val result = restClient.post("/greetings/withOption", body=JSON_EXAMPLE)
    assert(result === "Got None")
  }

  override protected def beforeAll() {
    Context.server.start()
  }

  override protected def afterAll() {
    Context.server.stop()
  }
}